export class CartItem {
  constructor(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
    quantity
  ) {
    this.product = {
      id: id,
      name: name,
      price: price,
      screen: screen,
      backCamera: backCamera,
      frontCamera: frontCamera,
      img: img,
      desc: desc,
      type: type,
    };
    this.quantity = quantity;
  }
}
